
from flask import Flask, jsonify
app = Flask(__name__)

@app.route('/')
def index():
    return jsonify({'msg': 'Test APP', 'Endpoints': ['/api/v1/users', '/api/v1/hello', '/api/v1/hello/<name>']})

@app.route('/api/v1/users')
def users():
    userlist = [
        {'name': 'Will', 'gender': 'Male'},
        {'name': 'Ako', 'gender': 'Female'},
        {'name': 'Seth', 'gender': 'Male'},
        {'name': 'Lily', 'gender': 'Female'},
        {'name': 'Claire', 'gender': 'Female'},
    ]
    return jsonify({'msg': 'Example userlist', 'data': userlist})

@app.route('/api/v1/hello')
@app.route('/api/v1/hello/')
@app.route('/api/v1/hello/<name>')
def hello(name=""):
    if name == "":
        return jsonify({'msg': 'Hello There'})
    return jsonify({'msg': f'Hello {name}', 'query': name})

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)